package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.enumerations.UserRole;
import com.nlmk.evteev.tm.repository.UserRepository;

import java.util.List;

/**
 * Сервисный класс для работы с репозиторием пользователя
 */
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Создание пользователя
     *
     * @param firstName  фамилия
     * @param secondName отчество
     * @param middleName имя
     * @param loginName  логон пользователя
     * @param password   пароль
     * @param userRole   признак администратора
     * @return созданный в системе пользователь {@link User}
     */
    public User create
    (
        final String loginName, final String password, final UserRole userRole,
        final String firstName, final String middleName, final String secondName
    ) {
        if (checkUserCredentials(loginName, password, userRole, firstName, middleName, secondName)) return null;
        return userRepository.create(loginName, password, userRole, firstName, middleName, secondName);
    }

    public User create(final String loginName, final String password, UserRole userRole) {
        if (loginName == null || loginName.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (userRole == null) userRole = UserRole.USER;
        return userRepository.create(loginName, password, userRole);
    }

    /**
     * Изменение пользователя
     *
     * @param firstName  фамилия
     * @param secondName отчество
     * @param middleName имя
     * @param loginName  логон пользователя
     * @param password   пароль
     * @param userRole   роль пользователя
     * @return пользователь {@link User}
     */
    public User update
    (
        final String loginName, final String password, final UserRole userRole,
        final String firstName, final String middleName, final String secondName
    ) {
        if (checkUserCredentials(loginName, password, userRole, firstName, middleName, secondName)) return null;
        return userRepository.update(loginName, password, userRole, firstName, middleName, secondName);
    }

    /**
     * Обновление пользователя
     *
     * @param user сущность пользователя для обновления
     * @return пользователь
     * @see User
     */
    public User update(final User user) {
        if (user == null) return null;
        return userRepository.update(user);
    }

    /**
     * Удаление пользователя
     *
     * @param loginName логин пользователя
     * @return удаленный пользователь {@link User}
     * или NULL, если такого не существует
     */
    public User deleteUserByLogin(final String loginName) {
        if (loginName == null || loginName.isEmpty()) return null;
        return userRepository.deleteUserByLogin(loginName);
    }

    /**
     * Проверка вводимых данных
     *
     * @param firstName  фамилия
     * @param secondName отчество
     * @param middleName имя
     * @param loginName  логон пользователя
     * @param password   пароль
     * @param userRole   роль пользователя
     * @return истина, если данные корректны, иначе ложь
     */
    private boolean checkUserCredentials
    (
        final String loginName, final String password, final UserRole userRole,
        final String firstName, final String middleName, final String secondName
    ) {
        if (firstName == null || firstName.isEmpty()) return false;
        if (middleName == null || middleName.isEmpty()) return false;
        if (secondName == null || secondName.isEmpty()) return false;
        if (loginName == null || loginName.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        return userRole == null;
    }

    /**
     * Возвращает пользователя по его логину
     *
     * @param loginName логин пользователя
     * @return пользователь {@link User} или NULL, если не найден
     */
    public User getUserByloginName(final String loginName) {
        if (loginName == null || loginName.isEmpty()) return null;
        return userRepository.getUserByloginName(loginName);
    }

    /**
     * Удаление пользователя
     *
     * @param userId логин пользователя
     * @return удаленный пользователь {@link User}
     * или NULL, если такого не существует
     */
    public User deleteUserById(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return userRepository.deleteUserById(userId);
    }

    /**
     * Поиск пользователя по ID
     *
     * @param userId код пользователя
     * @return пользователь с данным ID, либо NULL, если не найден
     */
    public User getUserById(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return userRepository.getUserById(userId);
    }

    /**
     * Список пользователей
     *
     * @return список пользователей в системе {@link List}
     * @see User
     */
    public List<User> findAll() {
        return userRepository.findAll();
    }

}
