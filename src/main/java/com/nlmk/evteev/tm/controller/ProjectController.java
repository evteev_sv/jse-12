package com.nlmk.evteev.tm.controller;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.service.ProjectService;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Класс контроллера проектов
 */
public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    /**
     * Конструктор контроллера
     *
     * @param projectService класс сервииса проектов {@link ProjectService}
     */
    protected ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Удаление проекта по имени
     *
     * @return код исполнения
     */
    public int removeProjectByName() {
        System.out.println("[Remove project by name]");
        System.out.println("Введите имя проекта: ");
        final String name = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(name))
            return TaskManagerUtil.printAndReturnFail("Название проекта не может быть пустым.");
        Project project = projectService.findByName(name);
        if (!TaskManagerUtil.checkProjectPrivs(project))
            return TaskManagerUtil.printAndReturnFail("Проект с таким названием не принадлежит пользователю.");
        project = projectService.removeByName(name);
        if (project == null)
            return TaskManagerUtil.printAndReturnFail("Проект с таким названием не найден.");
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление проекта по коду
     *
     * @return код исполнения
     */
    public int removeProjectById() {
        System.out.println("[Remove project by id]");
        System.out.println("Введите ID проекта: ");
        final Long vId = scanner.nextLong();
        Project project = projectService.findById(vId);
        if (!TaskManagerUtil.checkProjectPrivs(project))
            return TaskManagerUtil.printAndReturnFail("Проект с таким ID не принадлежит пользователю.");
        project = projectService.removeById(vId);
        if (project == null)
            return TaskManagerUtil.printAndReturnFail("Проекта с таким ID не найдено!");
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление проекта по индексу
     *
     * @return код исполнения
     */
    public int removeProjectByIndex() {
        System.out.println("[Remove project by index]");
        System.out.println("Введите индекс проекта: ");
        final int index = scanner.nextInt();
        Project project = projectService.findByIndex(index);
        if (!TaskManagerUtil.checkProjectPrivs(project))
            return TaskManagerUtil.printAndReturnFail("Проект с таким ID не принадлежит пользователю.");
        project = projectService.removeByIndex(index);
        if (project == null)
            return TaskManagerUtil.printAndReturnFail("Проекта с таким ID не найдено!");
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Изменение проекта по коду
     *
     * @return код исполнения
     */
    public int updateProjectById() {
        System.out.println("[Update project by id]");
        System.out.println("Введите код проекта: ");
        final Long vId = Long.getLong(scanner.nextLine());
        System.out.println("Введите новое имя проекта: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание проекта: ");
        final String description = scanner.nextLine();
        if (projectService.update(vId, name, description) == null)
            return TaskManagerUtil.printAndReturnFail("Ошибка обновления проекта.");
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Изменение проекта по индексу
     *
     * @return код исполнения
     */
    public int updateProjectByIndex() {
        System.out.println("[Update project by index]");
        System.out.println("Введите индекс проекта: ");
        final int vId = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectService.findByIndex(vId);
        if (project == null)
            return TaskManagerUtil.printAndReturnFail("Проект по индексу не найден!");
        System.out.println("Введите новое имя проекта: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание проекта: ");
        final String description = scanner.nextLine();
        if (projectService.update(project.getId(), name, description) == null)
            return TaskManagerUtil.printAndReturnFail("Изменение проекта не удалось!");
        return TaskManagerUtil.printAndReturnOk();
    }


    /**
     * Просмотр проекта по индексу
     *
     * @return код исполнения
     */
    public int viewProjectByIndex() {
        System.out.println("Введите индекс проекта: ");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null)
            return TaskManagerUtil.printAndReturnFail("Проект по индексу не найден.");
        viewProject(project);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Просмотр проекта в консоли
     *
     * @param project проект {@link com.nlmk.evteev.tm.entity.Project}
     */
    public int viewProject(final Project project) {
        if (project == null)
            return TaskManagerUtil.printAndReturnFail("Проект не определен!");
        System.out.println("[View Project]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.print("OWNER: ");
        UserController.getInstance().viewUserById(project.getOwnedUser().toString());
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Поиск проекта по коду
     *
     * @param projectId код проекта
     * @return проект {@link Project}
     */
    public Project findProjectById(final Long projectId) {
        if (projectId == null) return null;
        Project project = projectService.findById(projectId);
        if (!TaskManagerUtil.checkProjectPrivs(project))
            return null;
        return project;
    }

    /**
     * Создание проекта
     *
     * @return код выполнения
     */
    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Укажите имя проекта: ");
        final String lv_name = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(lv_name))
            return TaskManagerUtil.printAndReturnFail("Имя проекта не может быть пустым!");
        projectService.create(lv_name, UserController.getInstance().getAppUser().getUserId());
        return TaskManagerUtil.printAndReturnOk();
    }

    public int createProject(final String projectName) {
        if (!TaskManagerUtil.checkEmptyInput(projectName))
            return TaskManagerUtil.printAndReturnFail("Имя проекта не может быть пустым!");
        projectService.create(projectName, null);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Очистка проекта
     *
     * @return код выполнения
     */
    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        if (!UserController.getInstance().getAppUser().isAdmin()) {
            List<Project> projectList = projectService.findByUserId(UserController.getInstance().getAppUser().getUserId());
            if (projectList.isEmpty())
                return TaskManagerUtil.printAndReturnFail("У данного пользователя нет ни одного проекта.");
            projectService.clearListProject(projectList);
            return TaskManagerUtil.printAndReturnOk();
        }
        projectService.clear();
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Список проектов
     *
     * @return код выполнения
     */
    public int listProject() {
        System.out.println("[LIST PROJECT]");
        List<Project> projectList = projectService.findAll();
        if (!UserController.getInstance().getAppUser().isAdmin())
            projectList = projectService.findAll().stream().filter(project -> TaskManagerUtil.checkProjectPrivs(project)).collect(Collectors.toList());
        System.out.println(projectList);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Добавление задачи к проекту
     *
     * @param projectId код проекта
     * @param taskId    код задачи
     * @return код исполнения
     */
    public int addTaskToProject(final Long projectId, final Long taskId) {
        if (projectId == null) return TaskManagerUtil.printAndReturnFail("Код проекта не определен.");
        if (taskId == null) return TaskManagerUtil.printAndReturnFail("Код задачи не определен.");
        Project project = projectService.findById(projectId);
        if (project == null) return TaskManagerUtil.printAndReturnFail("Проект по коду не найден.");
        if (!TaskManagerUtil.checkProjectPrivs(project))
            return TaskManagerUtil.printAndReturnFail("Проект не принадлежит данному пользователю.");
        project.getTasks().add(taskId);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи из проекта
     *
     * @param projectId код проекта
     * @param taskId    код задачи
     * @return код исполнения
     */
    public int removeTaskFromProject(final Long projectId, final Long taskId) {
        if (projectId == null) return TaskManagerUtil.printAndReturnFail("Код проекта должен быть не пустой.");
        if (taskId == null) return TaskManagerUtil.printAndReturnFail("Код задачи не определен.");
        Project project = projectService.findById(projectId);
        if (project == null) return TaskManagerUtil.printAndReturnFail("Проект по коду не найден.");
        if (!TaskManagerUtil.checkProjectPrivs(project))
            return TaskManagerUtil.printAndReturnFail("Проект не принадлежит данному пользователю.");
        project.getTasks().remove(taskId);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Назначение пользователя на проект
     *
     * @return код исполнения
     */
    public int assignProjectToUser() {
        if (!UserController.getInstance().getAppUser().isAdmin())
            return TaskManagerUtil.printAndReturnFail("Вы не имеете прав на смену владельца для проекта!");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null)
            return TaskManagerUtil.printAndReturnFail("Код проекта не верен!");
        System.out.println("Введите код нового владельца проекта:");
        UUID uuid = UUID.fromString(scanner.nextLine());
        if (uuid == null)
            return TaskManagerUtil.printAndReturnFail("Код пользователя не верен!");
        projectService.updateProjectOwner(projectId, uuid);
        return TaskManagerUtil.printAndReturnOk();
    }

}
